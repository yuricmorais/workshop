package beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.Aluno;

@ManagedBean
@SessionScoped
public class AlunoBean {

	private Aluno aluno;
	private List<Aluno> alunos = new ArrayList<Aluno>();
	
	public AlunoBean(){
		aluno = new Aluno();
	}
	
	
	public String salvar(){
		alunos.add(aluno);
		System.out.println("Adicionando Aluno");
		return "lista?faces-redirect=true";
	}
	
	public String editar(Aluno aluno){
		this.aluno = aluno;
		System.out.println("Atualizando Aluno");
		return "cadastro?faces-redirect=true";
	}
	
	public String remove(Aluno aluno){
		this.aluno = aluno;
		alunos.remove(aluno);
		System.out.println("Aluno Removido");
		return "lista?faces-redirect=true";
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

}
